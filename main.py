from fastapi import Depends, FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.exceptions import HTTPException
from app.admin.config import SessionAdmin

from app.users.dependencies import get_current_user
from app.users.models import User
from app.users.router import router as router_users
from app.main.router import router as router_main
from app.ext.info.router import router as router_info
from app.ext.wiki.router import router as router_wiki
from app.ext.warehouse.router import router as router_warehouse
from app.ext.script.router import router as router_script
from app.admin.views import UserAdmin, HardwareAdmin, HardwareExtAdmin, InfoScriptAdmin, WikiGroupAdmin, WarehouseAdmin
from app.admin.auth import authentication_backend
from app.config.config import settings
from app.config.flash import flash, get_flashed_messages

from sqladmin import Admin

from starlette.middleware import Middleware
from starlette.middleware.sessions import SessionMiddleware
import uvicorn




middleware = [
    Middleware(SessionMiddleware, secret_key=settings.SECRET_KEY)
]


app = FastAPI(middleware=middleware)
app.include_router(router_users)
app.include_router(router_main)
app.include_router(router_info)
app.include_router(router_wiki)
app.include_router(router_warehouse)
app.include_router(router_script)
app.mount('/static', StaticFiles(directory='app/static'), name='static')
templates = Jinja2Templates(directory="app/templates")
templates.env.globals['get_flashed_messages'] = get_flashed_messages


@app.exception_handler(403)
async def exc403handler(request: Request, exc: HTTPException):
    return RedirectResponse('/403')

@app.get('/403', response_class=HTMLResponse)
async def exc403(request:Request):
    flash(request, 'Ваш токен истек, войдите на сайт.', 'alert-danger')
    return RedirectResponse('auth/signin')


@app.exception_handler(404)
async def exc404handler(request: Request, exc: HTTPException):
    return RedirectResponse("/404")

@app.get('/404', response_class=HTMLResponse)
async def exc404(request:Request, current_user: User = Depends(get_current_user)):
    data_main = {'title': '404'}
    return templates.TemplateResponse("main/404.html", {"request":request, 'data_main': data_main, 'user': current_user})



admin = Admin(app, session_maker=SessionAdmin, authentication_backend=authentication_backend)
admin.add_view(UserAdmin)
admin.add_view(HardwareAdmin)
admin.add_view(HardwareExtAdmin)
admin.add_view(WarehouseAdmin)
admin.add_view(InfoScriptAdmin)
admin.add_view(WikiGroupAdmin)




if __name__ == '__main__':
    uvicorn.run(
        "main:app", 
        host="127.0.0.1", 
        port=8000, 
        log_level="info", 
        reload=True, 
        workers=4,        
        ssl_certfile="./certificates/root-ca.crt",
        ssl_keyfile="./certificates/root-ca.key",
        )