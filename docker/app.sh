#!/bin/bash



# gunicorn main:app --workers 8 --worker-class uvicorn.workers.UvicornWorker --bind=0.0.0.0:8888


gunicorn main:app --workers 8 --worker-class uvicorn.workers.UvicornWorker --bind=0.0.0.0:8888 --certfile="/main/certificates/root-ca.crt" --keyfile="/main/certificates/root-ca.key"