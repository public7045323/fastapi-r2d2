from celery import Celery
from app.config.config import settings


celery = Celery(
    'celery_tasks',
    broker = f"redis://{settings.REDIS_HOST}:{settings.REDIS_PORT}",
    broker_api = f"redis://{settings.REDIS_HOST}:{settings.REDIS_PORT}/api",
    broker_connection_retry_on_startup = True,
    include = ["app.celery.celery_tasks"]
)


