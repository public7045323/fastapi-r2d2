from email.message import EmailMessage
from app.config.config import settings
from pydantic import EmailStr
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def email_pass_reset(
    www: str,    
    link_reset: str,
    email_to: EmailStr,
):
    # Параметры SMTP-сервера
    smtp_server = settings.SMTP_HOST
    port = settings.SMTP_PORT
    sender_email = settings.SMTP_USER
    password = settings.SMTP_PASS

    # print(settings.SMTP_HOST, settings.SMTP_PORT, settings.SMTP_USER, settings.SMTP_PASS)

    # Создание сообщения
    message = MIMEMultipart("alternative")
    message["Subject"] = f"Запрос на сброс пароля на сайте {www}"
    message["From"] = sender_email
    message["To"] = email_to


    # Текст письма
    # text = """
    # Привет,
    # Как дела???"""

    html = f"""
    <html>
        <body>
            <h2> Запрос на сброс пароля на сайте {www}. </h2> 
            <p> Для сброса пароля перейдите по ссылке: </p>
            <p> {link_reset} </p>
            <p> ----------------------------------------------</p>
            <p> Если вы не запрашивали сброс пароля, то проигнорируйте это письмо. </p>
        </body>
    </html>
    """

    # Добавление текста в сообщение
    # part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")
    # message.attach(part1)
    message.attach(part2)

    # Отправка сообщения
    try:
        server = smtplib.SMTP(smtp_server, port)
        server.starttls()  # Начало защищенного соединения
        server.login(sender_email, password)
        server.sendmail(sender_email, email_to, message.as_string())
        print("Письмо успешно отправлено")
    except Exception as e:
        print(f"Ошибка при отправке письма: {e}")
    finally:
        server.quit()