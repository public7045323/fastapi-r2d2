from pydantic import EmailStr
from app.celery.celery import celery
from app.config.config import settings
from app.celery.email_templates import email_pass_reset



@celery.task
def send_email_pass_reset(
    www: str,    
    link_reset: str,
    email_to: EmailStr,
):
    email_pass_reset(www, link_reset, email_to)