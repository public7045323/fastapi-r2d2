from sqladmin import ModelView
from fastapi import Request
from app.users.dependencies import get_current_user
from app.users.models import User
from app.ext.hardware.models import Hardware, Hardware_ext
from app.ext.info.models import InfoScript
from app.ext.wiki.models import Wiki_Group
from app.ext.warehouse.models import Warehouse



class UserAdmin(ModelView, model=User):
    async def is_accessible(self, request = Request) -> bool:
        user = await get_current_user(request.session["token"])
        if user.is_admin or "users_crud" in user.access.split(", "):
            return True
        else:
            return False
        

    # column_details_exclude_list=[User.hashed_password]
    name = 'Пользователь'
    name_plural = 'Пользователи'
    icon = 'fa-solid fa-users'
    page_size = 20
    page_size_options = [10, 25, 50, 100]
    can_delete = True
    column_default_sort = ("id", True)
    column_searchable_list = [User.name, User.email]

    column_list = [
        User.id, 
        User.name, 
        User.email, 
        User.access,
        User.is_active,
        User.is_admin, 
        User.is_sqladmin,
        User.tz,
        User.entered_at,
        User.registered_at
    ]
    column_sortable_list = [
        User.id,
        User.name,
        User.email,
        User.access,
        User.is_active,
        User.is_admin,
        User.is_sqladmin,
        User.tz,
        User.entered_at,
        User.registered_at
    ]
    column_labels = {
        User.id: "#",
        User.name: "Имя", 
        User.email: "Почта",
        User.access: 'Доступ',
        User.is_active: 'Активен',
        User.is_admin: 'Админ',
        User.is_sqladmin: 'SQL Админ',
        User.tz: 'Часовй пояс',
        User.entered_at: 'Вход',
        User.registered_at: 'Зарегестрирован'
    }
    can_export = False


class HardwareAdmin(ModelView, model=Hardware):
    async def is_accessible(self, request = Request) -> bool:
        user = await get_current_user(request.session["token"])
        if user.is_admin or "hardware_crud" in user.access.split(", "):
            return True
        else:
            return False

    column_list = [c.name for c in Hardware.__table__.c] + [Hardware.hardware_ext]
    column_default_sort = ("id", True)
    column_searchable_list = [Hardware.name, Hardware.id]
    name = 'Оборудование'
    name_plural = 'Оборудование'
    icon = 'fa-solid fa-layer-group'

    column_labels = {
        Hardware.id: '#',
        Hardware.name: 'Название', 
        Hardware.ip: 'IP-адрес',
        Hardware.added_at: 'Добавлено',
        Hardware.hardware_ext: 'Статус +'
    }


class HardwareExtAdmin(ModelView, model=Hardware_ext):
    async def is_accessible(self, request = Request) -> bool:
        user = await get_current_user(request.session["token"])
        if user.is_admin or "hardware_crud" in user.access.split(", "):
            return True
        else:
            return False

    column_list = [c.name for c in Hardware_ext.__table__.c] + [Hardware_ext.hardware]
    column_searchable_list = [Hardware_ext.desc]
    column_default_sort = ("id", True)
    name = 'Оборудование +'
    name_plural = 'Оборудование +'
    icon = 'fa-solid fa-layer-group'

    column_labels = {
        Hardware_ext.id: '#',
        Hardware_ext.status: 'Статус',
        Hardware_ext.desc: 'Описание',
        Hardware_ext.backup_cfg: 'Бекап конфиги?',
        Hardware_ext.hardware_fk: '# +',
        Hardware_ext.hardware: 'Связан с' 
    }


class InfoScriptAdmin(ModelView, model=InfoScript):
    async def is_accessible(self, request = Request) -> bool:
        user = await get_current_user(request.session["token"])
        if user.is_admin or "info_scripts_crud" in user.access.split(", "):
            return True
        else:
            return False

    column_list = [c.name for c in InfoScript.__table__.c]
    column_searchable_list = [InfoScript.id, InfoScript.log]
    column_default_sort = ("id", True)
    column_formatters = {InfoScript.log: lambda l, a: l.log[:100]}
    name = 'Лог'
    name_plural = 'Логи'
    icon = 'fa-solid fa-comment'
    page_size = 12
    page_size_options = [10, 25, 50, 100]

    column_sortable_list = [
        InfoScript.id,
        InfoScript.name_script,
        InfoScript.log,
        InfoScript.date,
    ]

    column_labels = {
        InfoScript.id: '#',
        InfoScript.name_script: 'Название скрипта',
        InfoScript.log: 'Лог',
        InfoScript.date: 'Дата',
    }


class WikiGroupAdmin(ModelView, model=Wiki_Group):
    async def is_accessible(self, request = Request) -> bool:
        user = await get_current_user(request.session["token"])
        if user.is_admin or "wiki_groups_crud" in user.access.split(", "):
            return True
        else:
            return False
    
    column_list = [c.name for c in Wiki_Group.__table__.c]
    column_searchable_list = [Wiki_Group.id, Wiki_Group.name, Wiki_Group.resource]
    column_default_sort = ("id", True)
    # column_formatters = {Wiki_Group.log: lambda l, a: l.log[:100]}
    name = 'Вики: группа'
    name_plural = 'Вики: группы'
    icon = 'fa-solid fa-user-group'
    page_size = 12
    page_size_options = [10, 25, 50, 100]

    column_sortable_list = [
        Wiki_Group.id,
        Wiki_Group.root_id,
        Wiki_Group.name,
        Wiki_Group.resource,
        Wiki_Group.ip,
        Wiki_Group.agreement,
        Wiki_Group.responsible,
        Wiki_Group.object,
        Wiki_Group.description,
    ]

    column_labels = {
        Wiki_Group.id: 'ID',
        Wiki_Group.root_id: 'Root_ID',
        Wiki_Group.name: 'Название группы',
        Wiki_Group.resource: 'Ресурс',
        Wiki_Group.ip: 'IP-адрес',
        Wiki_Group.agreement: 'Согласование?',
        Wiki_Group.responsible: 'Ответственный',
        Wiki_Group.object: 'Применение',
        Wiki_Group.description: 'Описание',
    }


class WarehouseAdmin(ModelView, model=Warehouse):
    column_list = [
        Warehouse.id, 
        Warehouse.name, 
        Warehouse.quantity, 
        Warehouse.desc_in,
        Warehouse.desc_out,
        Warehouse.status, 
        Warehouse.created_at,
        Warehouse.updated_at,
    ]

    can_delete = True
    name = 'Склад'
    name_plural = 'Склад'
    icon = 'fa-solid fa-cube'
    page_size = 20
    page_size_options = [10, 25, 50, 100]
    column_searchable_list = [Warehouse.name, Warehouse.desc_in, Warehouse.desc_out]
    column_sortable_list = [
        Warehouse.id, 
        Warehouse.name, 
        Warehouse.quantity, 
        Warehouse.desc_in,
        Warehouse.desc_out,
        Warehouse.status, 
        Warehouse.created_at,
        Warehouse.updated_at,
    ]
    column_default_sort = ("id", True)
    column_labels = {
        Warehouse.id: "#",
        Warehouse.name: "Наименование", 
        Warehouse.quantity: 'Количество',
        Warehouse.desc_in: 'Описание вх',
        Warehouse.desc_out: 'Описание вых',
        Warehouse.status: 'Статус',
        Warehouse.created_at: 'Дата создания',
        Warehouse.updated_at: 'Дата редактирования',
    }
    can_export = True