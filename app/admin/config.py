from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker
from app.config.config import settings
from app.users.models import User
from app.ext.hardware.models import Hardware, Hardware_ext
from app.ext.info.models import InfoScript
from app.ext.wiki.models import Wiki_Group
from app.ext.warehouse.models import Warehouse




DATABASE_URL = settings.DATABASE_URL
DATABASE_PARAMS_USERS = {}
engine_base_main = create_async_engine(DATABASE_URL, **DATABASE_PARAMS_USERS)


SessionAdmin = async_sessionmaker()
SessionAdmin.configure(binds={
    User: engine_base_main, 
    Hardware: engine_base_main,
    Hardware_ext: engine_base_main,
    InfoScript: engine_base_main,
    Wiki_Group: engine_base_main,
    Warehouse: engine_base_main,
    }
)