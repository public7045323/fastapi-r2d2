from fastapi import APIRouter, Depends, Request
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from app.celery.celery_tasks import send_email_pass_reset
from app.users.dependencies import get_current_user
from app.users.models import User
from app.users.schemas import SPassReset, SPassResetId, SUsersAuthIn, SUsersAuthUp, SPassChange, SMeEdit
from app.users.dto import UsersDTO
from app.users.auth import authenticate_user, get_password_hash, create_access_token, \
    get_password_reset_token, verify_password, verify_password_reset_token
from starlette.responses import HTMLResponse
from app.config.flash import flash, get_flashed_messages
from app.config.config import settings
from app.config import jinja_filters
from datetime import datetime



router = APIRouter(prefix='/auth', tags=['Users'])
templates=Jinja2Templates(directory="app/templates/")
templates.env.globals['get_flashed_messages'] = get_flashed_messages
templates.env.filters["my_date_filter"] = jinja_filters.datetime_format_tz


@router.post('/signup')
async def signup_post(request:Request, user_data: SUsersAuthUp=Depends(SUsersAuthUp.as_form)):
    existing_user = await UsersDTO.find_one_or_none(email=user_data.email)
    if existing_user:
        flash(request, f'Пользовователь с почтой {user_data.email} уже существует.', 'alert-danger')
        return RedirectResponse(url="/auth/signup/", status_code=303)
    if len(user_data.password) <= 7:
        flash(request, f'Длина пароля должна быть минимум 8 символов.', 'alert-danger')
        return RedirectResponse(url="/auth/signup/", status_code=303)
    if user_data.password != user_data.password2:
        flash(request, f'Пароли не совпадают.', 'alert-danger')
        return RedirectResponse(url="/auth/signup/", status_code=303)
    hashed_password = get_password_hash(user_data.password)
    await UsersDTO.add(name=user_data.name, email=user_data.email, hashed_password=hashed_password)
    flash(request, 'Вы успешно зарегистрировались, авторизируйтесь.', 'alert-info')
    
    return RedirectResponse(url='/auth/signin', status_code=303)


@router.get('/signup', response_class=HTMLResponse)
def signup_get(request:Request):
    data_main = {'title': 'Регистрация'}
    
    return templates.TemplateResponse("auth/signup.html", {"request":request, 'data_main': data_main})


@router.post('/signin')
async def signin_post(request:Request, user_data: SUsersAuthIn=Depends(SUsersAuthIn.as_form)):
    user = await authenticate_user(user_data.email, user_data.password)
    if not user:
        flash(request, 'Почта или пароль введены неверно.', 'alert-danger')
        
        return RedirectResponse(url="/auth/signin/", status_code=303)
    
    await UsersDTO.update(id=user.id, entered_at=datetime.utcnow())
    access_token = create_access_token({"sub": str(user.id)})
    response = RedirectResponse(url="/", status_code=303)
    response.set_cookie(settings.TOKEN, access_token, httponly=True)

    return  response


@router.get("/signin", response_class=HTMLResponse)
def signin_get(request:Request):
    data_main = {'title': 'Вход'}
    
    return templates.TemplateResponse("auth/signin.html", {"request":request, 'data_main': data_main})


@router.get("/logout", response_class=HTMLResponse)
async def logout():
    response = RedirectResponse(url='/', status_code=303)
    response.delete_cookie(settings.TOKEN)
    
    return response


@router.get("/me")
async def read_user_me(request:Request, current_user: User = Depends(get_current_user)):
    data_main = {'title': 'Обо мне'}

    return templates.TemplateResponse("auth/me.html", {"request":request, 'data_main': data_main, 'user': current_user})


@router.get('/me_edit')
async def me_edit_get(
    request:Request, current_user: 
    User = Depends(get_current_user)
):
    data_main = {'title': 'Редактирование'}

    if not current_user:
        flash(request, 'Вы не зарегистрированы.', 'alert-danger')
        return RedirectResponse(url="/auth/signin/", status_code=303)

    return templates.TemplateResponse('auth/me_edit.html', {
                                                            'request':request, 
                                                            'user': current_user,
                                                            'data_main': data_main
                                                            })


@router.post('/me_edit')
async def me_edit_post(
    request:Request, 
    current_user: 
    User = Depends(get_current_user), 
    me_data: SMeEdit=Depends(SMeEdit.as_form)
):
    if not current_user:
        flash(request, 'Вы не зарегистрированы.', 'alert-danger')
        return RedirectResponse(url="/auth/signin/", status_code=303)

    if not verify_password(me_data.password, current_user.hashed_password):
        flash(request, 'Текущий пароль введен неверно.', 'alert-danger')
        return RedirectResponse(request.url_for('me_edit_get'), status_code=303)
    
    if current_user.tz == me_data.tz and current_user.name == me_data.name and current_user.email == me_data.email:
        flash(request, 'Нечего изменять.', 'alert-danger')
    else:
        await UsersDTO.update(id=current_user.id, tz=me_data.tz, name=me_data.name, email=me_data.email)
        flash(request, 'Ваши данные были изменены.', 'alert-danger')
    
    response = RedirectResponse(
        request.url_for('read_user_me'), 
        status_code=303
    )

    return response


@router.get('/pass_change')
async def pass_change_get(request:Request, current_user: User = Depends(get_current_user)):
    data_main = {'title': 'Смена пароля'}

    if not current_user:
        flash(request, 'Вы не зарегистрированы.', 'alert-danger')
        return RedirectResponse(url="/auth/signin/", status_code=303)

    return templates.TemplateResponse('auth/pass_change.html', {
                                                                'request':request, 
                                                                'user': current_user,
                                                                'data_main': data_main
                                                                })


@router.post('/pass_change')
async def pass_change_post(
    request:Request, 
    current_user: User = Depends(get_current_user), 
    user_data: SPassChange=Depends(SPassChange.as_form)
):
    if not current_user:
        flash(request, 'Вы не зарегистрированы.', 'alert-danger')
        return RedirectResponse(url="/auth/signin/", status_code=303)

    if not verify_password(user_data.password_now, current_user.hashed_password):
        flash(request, 'Текущий пароль введен неверно.', 'alert-danger')
        return RedirectResponse(request.url_for('pass_change_get'), status_code=303)
    
    if user_data.password == user_data.password2:
        pass_new = get_password_hash(user_data.password)
    else:
        flash(request, 'Пароли не совадают.', 'alert-danger')
        return RedirectResponse(request.url_for('pass_change_get'), status_code=303)
    
    if len(user_data.password) <= 7:
        flash(request, f'Длина пароля должна быть минимум 8 символов.', 'alert-danger')
        return RedirectResponse(request.url_for('pass_change_get'), status_code=303)

    await UsersDTO.update_pass(email=current_user.email, hashed_password=pass_new)
    flash(request, 'Пароль изменен, войдите повторно', 'alert-danger')
    
    response = RedirectResponse(url='/auth/signin', status_code=303)
    response.delete_cookie(settings.TOKEN)
    
    return response


@router.get('/pass_reset')
async def pass_reset_get(request:Request,):
    data_main = {'title': 'Сброс пароля'}
    return templates.TemplateResponse('auth/pass_reset.html', {
                                                            'request':request, 
                                                            'data_main': data_main
                                                            })


@router.post('/pass_reset')
async def pass_reset_get(request:Request, user_data: SPassReset=Depends(SPassReset.as_form)):
    data_base = await UsersDTO.find_one_or_none(email=user_data.email)
    if data_base:
        token_reset = await get_password_reset_token(data_base.id)
        send_email_pass_reset.delay(
            www= settings.WWW,
            link_reset=f'{settings.WWW}/auth/pass_reset/{token_reset}',
            email_to=data_base.email)
    
    flash(request, f'Если {user_data.email} есть в базе, то на нее выслали инструкцию по сбросу пароля.', 'alert-danger')
    response = RedirectResponse(request.url_for('pass_reset_get'), status_code=303)
    return response


@router.get('/pass_reset/{token_pass_reset}')
async def pass_reset_base_get(request:Request, token_pass_reset:str):
    data_main = {'title': 'Сброс пароля'}

    id = await verify_password_reset_token(token_pass_reset)
    if id:
        return templates.TemplateResponse(
            'auth/pass_reset_id.html',  
            {
                'request':request, 
                'data_main': data_main
            }
        )
    else:
        flash(request, 'Время жизни токена истекло.', 'alert-danger')
        response = RedirectResponse(
            request.url_for('pass_reset_get'), status_code=303)
        return response


@router.post('/pass_reset/{token_pass_reset}')
async def pass_reset_base_post(
    request:Request, 
    token_pass_reset:str, 
    user_data: SPassResetId=Depends(SPassResetId.as_form)
):
    id = await verify_password_reset_token(token_pass_reset)
    if id:
        if user_data.password != user_data.password2:
            flash(request, 'Пароли не совпадают.', 'alert-danger')
            response = RedirectResponse(
                request.url_for(
                    'pass_reset_base_get', 
                    token_pass_reset=token_pass_reset), 
                status_code=303)
            return response
        
        if len(user_data.password) <= 7:
            flash(request, f'Длина пароля должна быть минимум 8 символов.', 'alert-danger')
            return RedirectResponse(request.url_for('pass_reset_base_get', token_pass_reset=token_pass_reset), status_code=303)
        
        hashed_password = get_password_hash(user_data.password)
        await UsersDTO.update(id=id, hashed_password=hashed_password)
        
        flash(request, 'Пароль изменен, войдите на сайте с обновленным паролем.', 'alert-danger')
        return RedirectResponse(request.url_for('signin_post'), status_code=303)

    else:
        flash(request, 'Время жизни токена истекло.', 'alert-danger')
        response = RedirectResponse(
            request.url_for('pass_reset_get'), status_code=303)
        return response



