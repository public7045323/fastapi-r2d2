from fastapi import Depends, HTTPException, Request, Response
from jose import ExpiredSignatureError, JWTError, jwt

from app.config.config import settings
from app.users.dto import UsersDTO
from app.config.exceptions import IncorrectTokenFormatException, TokenExpiredException, UserIsNotPresentException




def get_token(request: Request):
    token = request.cookies.get(settings.TOKEN)
    if not token:
        # raise TokenAbsentException
        token = 'no_user'
        return token
    return token


async def get_current_user(token: str = Depends(get_token)):
    if token == 'no_user':
        user = None
        return user
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, settings.ALGORITHM
        )
    except ExpiredSignatureError:
        raise TokenExpiredException

    except JWTError:
        raise IncorrectTokenFormatException
    
    user_id: str = payload.get('sub')
    if not user_id:
        raise UserIsNotPresentException
    
    user = await UsersDTO.find_by_id(int(user_id))
    if not user:
        raise UserIsNotPresentException
    return user