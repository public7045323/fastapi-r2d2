from sqlalchemy import Text, BOOLEAN
from sqlalchemy.orm import Mapped, mapped_column
from app.config.database import Base




class Wiki_Group(Base):
    __tablename__ = "wiki_groups"
    id: Mapped[int] = mapped_column(primary_key=True)
    root_id: Mapped[int| None]
    name: Mapped[str] = mapped_column(unique=True)
    resource: Mapped[str] 
    ip: Mapped[str | None]
    agreement: Mapped[bool] = mapped_column(default=False)
    responsible: Mapped[str | None]
    object: Mapped[str | None]
    description: Mapped[str | None] = mapped_column(Text)



    def __str__(self):
        return f'Wiki_group: {self.name} - {self.resource}'
