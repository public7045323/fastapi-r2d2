import math
from sqlalchemy import desc, func, or_, select
from app.config.dto.base import BaseDTO
from app.ext.wiki.models import Wiki_Group




class WikiGroupDTO(BaseDTO):
    model = Wiki_Group
    
    @classmethod
    async def paginate_find(cls, page, size, q):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).where(or_(
                cls.model.__table__.columns.name.ilike(f'%{q}%'),
                cls.model.__table__.columns.resource.ilike(f'%{q}%'),
                )
            ).order_by(desc(cls.model.__table__.columns.id)).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__).where(or_(
                cls.model.__table__.columns.name.ilike(f'%{q}%'),
                cls.model.__table__.columns.resource.ilike(f'%{q}%'),
                )
            )
            
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records,
            'q': q,
            }
            
            return paginate
    
    @classmethod
    async def find_all(cls, **filter_by):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).filter_by(**filter_by).order_by(cls.model.__table__.columns.id)
            result = await session.execute(query)
            return result.all()

