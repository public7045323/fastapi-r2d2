from typing import Annotated
from fastapi import APIRouter, Depends, Request, Path
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from fastapi.params import Query

from app.config.flash import flash, get_flashed_messages
from app.config import jinja_filters
from app.users.dependencies import get_current_user
from app.users.models import User
from app.config.access import site_access
from app.ext.wiki.dto import WikiGroupDTO




router = APIRouter( prefix='/wiki', tags=['Wiki'])
templates=Jinja2Templates(directory="app/templates/")
templates.env.globals['get_flashed_messages'] = get_flashed_messages
templates.env.filters["my_date_filter"] = jinja_filters.datetime_format_tz


@router.get("/main",)
async def wiki_main_get(
    request: Request, 
    current_user: User = Depends(get_current_user),
):
    data_main= {'title': 'Вики'}

    access = await site_access(request, current_user, 'wiki_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))

    return templates.TemplateResponse(
        'wiki/wiki_main.html', 
        {'request': request, 'data_main': data_main, 'user': current_user}
    )

###############################################################################

@router.get("/groups",)
async def wiki_groups_get(
    request: Request, 
    current_user: User = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=50, default=14),
    q: str | None = Query(default=None), 
):
    data_main= {'title': 'Вики: Группы'}

    access = await site_access(request, current_user, 'wiki_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    paginate = await WikiGroupDTO.paginate(page, size)
    if paginate is None:
        flash(request, 'База пуста.', 'alert-info')

    # print(paginate)

    return templates.TemplateResponse(
        'wiki/wiki_groups.html', 
        {'request': request, 'data_main': data_main, 'user': current_user, 'paginate': paginate}
    )

#################################################################################################

@router.get('/search/')
async def wiki_groups_search(
    request: Request, 
    current_user: User = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=50, default=14),
    q: str | None = Query(default=None), 
):
    data_main = {'title': 'Поиск по wiki группам'}
    access = await site_access(request, current_user, 'wiki_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    if q:
        q = q.strip()

    else:
        return RedirectResponse(request.url_for('wiki_groups_get'), status_code=303)
    
    paginate = await WikiGroupDTO.paginate_find(page, size, q)
    if not paginate:
        flash(request, f'По вашему запросу *** {q} *** ничего не найдено.', 'alert-warning')
        return RedirectResponse(request.url_for('wiki_groups_get'), status_code=303)
    
    
    return templates.TemplateResponse(
        'wiki/wiki_groups.html', 
        {'request': request, 'data_main': data_main, 'user': current_user, 'paginate': paginate}
    )

####################################################################################################

@router.get("/groups/{group_id}")
async def wiki_groups_chain_get(
    group_id: Annotated[int, Path(ge=1,)],
    request:Request,
    current_user: User = Depends(get_current_user),
    # q: str | None = Query(default=None),
):
    data_main = {'title': 'Вики цепочка групп'}
    access = await site_access(request, current_user, 'wiki_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    # if q:
    #     q = q.strip()
    # else:
    #     print('test chain')
    #     return RedirectResponse(request.url_for('wiki_groups_get'), status_code=303)

    data_base = {}
    group_base = await WikiGroupDTO.find_one_or_none(id = group_id)
    if group_base and group_base.root_id == None:
        data_base['main'] = group_base

        return templates.TemplateResponse(
        'wiki/wiki_groups_chain.html', 
        {'request': request, 'data_main': data_main, 'user': current_user, 'data_base': data_base}
        )

    elif group_base and group_base.root_id == 0:
        data_base['main'] = group_base
        groups_base_from_root_id = await WikiGroupDTO.find_all(root_id = group_base.id)
        if groups_base_from_root_id:
            data_base['secondary'] = groups_base_from_root_id

        return templates.TemplateResponse(
        'wiki/wiki_groups_chain.html', 
        {'request': request, 'data_main': data_main, 'user': current_user, 'data_base': data_base}
        )
    
    elif group_base and group_base.root_id > 0:
        group_base_root = await WikiGroupDTO.find_one_or_none(id = group_base.root_id)
        data_base['main'] = group_base_root
        groups_base_from_root_id = await WikiGroupDTO.find_all(root_id = group_base.root_id)
        if groups_base_from_root_id:
            data_base['secondary'] = groups_base_from_root_id

        return templates.TemplateResponse(
        'wiki/wiki_groups_chain.html', 
        {'request': request, 'data_main': data_main, 'user': current_user, 'data_base': data_base}
        )