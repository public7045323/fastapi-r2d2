from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column,relationship
from datetime import datetime
from typing import List

from app.config.database import Base



class Hardware(Base):
    __tablename__ = "hardware"
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    ip: Mapped[str| None]
    added_at: Mapped[datetime | None] = mapped_column(onupdate=datetime.utcnow())

    hardware_ext: Mapped["Hardware_ext"] = relationship(back_populates="hardware", uselist=False)

    def __str__(self):
        return f'Hardware : {self.name}, IP: {self.ip}'
    

class Hardware_ext(Base):
    __tablename__ = 'hardware_ext'
    id: Mapped[int] = mapped_column(primary_key=True)
    status: Mapped[bool] = mapped_column(default=False)
    desc: Mapped[str | None]
    backup_cfg: Mapped[bool | None]

    hardware_fk: Mapped[int] = mapped_column(ForeignKey("hardware.id", ondelete="CASCADE"))
    hardware : Mapped["Hardware"] = relationship(back_populates="hardware_ext", uselist=False)

    def __str__(self):
        return f'Hardware_ext : {self.status}'

