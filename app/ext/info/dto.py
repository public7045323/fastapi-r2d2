import math
from sqlalchemy import desc, func, or_, select
from app.config.dto.base import BaseDTO
from app.ext.info.models import InfoScript



class InfoScriptsDTO(BaseDTO):
    model = InfoScript

    @classmethod
    async def paginate_find(cls, page, size, q):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).where(or_(
                cls.model.__table__.columns.name_script.ilike(f'%{q}%'),
                cls.model.__table__.columns.log.ilike(f'%{q}%'),
                )
            ).order_by(desc(cls.model.__table__.columns.id)).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__).where(or_(
                cls.model.__table__.columns.name_script.ilike(f'%{q}%'),
                cls.model.__table__.columns.log.ilike(f'%{q}%'),
                )
            )
            
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records,
            'q': q,
            }
            
            return paginate
