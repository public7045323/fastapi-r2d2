from typing import Annotated
from fastapi import APIRouter, Depends, Request, Path
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from fastapi.params import Query

from app.config.flash import flash, get_flashed_messages
from app.config import jinja_filters
from app.config.access import site_access
from app.config.config import settings
from app.users.dependencies import get_current_user
from app.users.models import User
from app.ext.info.dto import InfoScriptsDTO

import re
import os.path
from datetime import datetime, timedelta



router = APIRouter( prefix='/info', tags=['Info'])
templates=Jinja2Templates(directory="app/templates/")
templates.env.globals['get_flashed_messages'] = get_flashed_messages
templates.env.filters["my_date_filter"] = jinja_filters.datetime_format_tz


@router.get("/info_scripts",)
async def info_scripts_get(
    request: Request, 
    current_user: User = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=50, default=17),
):
    data_main= {'title': 'Логи скриптов'}

    access = await site_access(request, current_user, 'info_scripts_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))

    paginate = await InfoScriptsDTO.paginate(page, size)
    if paginate is None:
        flash(request, 'База пуста.', 'alert-info')

    return templates.TemplateResponse(
        'info/info_scripts_all.html', 
        {'request': request, 'data_main': data_main, 'user': current_user, 'paginate': paginate}
    )


@router.get('/search/')
async def info_scripts_search(
    request: Request, 
    current_user: User = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=50, default=15),
    q: str | None = Query(default=None), 
):
    data_main = {'title': 'Поиск по логам скриптов'}
    access = await site_access(request, current_user, 'info_scripts_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    if q:
        q = q.strip()

    else:
        return RedirectResponse(request.url_for('info_scripts_get'), status_code=303)
    
    paginate = await InfoScriptsDTO.paginate_find(page, size, q)
    if not paginate:
        flash(request, f'По вашему запросу *** {q} *** ничего не найдено.', 'alert-warning')
        return RedirectResponse(request.url_for('info_scripts_get'), status_code=303)
    
    return templates.TemplateResponse(
        'info/info_scripts_all.html', 
        {'request': request, 'data_main': data_main, 'user': current_user, 'paginate': paginate}
    )


@router.get("/info_scripts/{info_scripts_id}")
async def info_scripts_id_get(
    info_scripts_id: Annotated[int, Path(ge=1,)],
    request:Request,
    current_user: User = Depends(get_current_user),
):
    data_main = {'title': 'Лог скрипта'}
    
    access = await site_access(request, current_user, 'info_scripts_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    data_base = await InfoScriptsDTO.find_by_id(info_scripts_id)
    
    return templates.TemplateResponse(
        'info/info_scripts_one.html',
        {"request":request, 'user': current_user, 'data_main': data_main, 'data_base': data_base}
    )

#####################################################################################################

@router.get("/proxy_pac",)
async def proxyPacGet(
    request: Request, 
    current_user: User = Depends(get_current_user),
):
    data_main= {'title': 'Прокси Пак'}

    access = await site_access(request, current_user, 'info_scripts_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))

    with open(f'{settings.TEMP_DIR}/main_wpad.dat', 'r') as file:
        proxy_pac: list = []
        for line in file:
            line = line.strip()
            if re.search('shExpMatch', line):
                line = line.replace('|', '')
                proxy_pac.append(f'   {line}')

            elif re.search('return "PROXY 10.105.0.217:3128"', line):
                proxy_pac.append("---> PROXY 10.105.0.217:3128")
                proxy_pac.append('')

            elif re.search('DIRECT', line):
                proxy_pac.append("---> DIRECT")
                
            else:
                pass

    proxy_pac_date = datetime.fromtimestamp(os.path.getmtime(os.path.join(settings.TEMP_DIR, 'main_wpad.dat'))) + timedelta(hours=current_user.tz)
    proxy_pac_date = str(proxy_pac_date).split('.')[0]

    data_main['proxy_pac'] = proxy_pac
    data_main['proxy_pac_date'] = proxy_pac_date

    return templates.TemplateResponse(
        'info/proxy_pac_index.html',
        {"request":request, 'user': current_user, 'data_main': data_main}
    )