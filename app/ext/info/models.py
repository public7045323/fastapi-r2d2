from sqlalchemy import Text
from sqlalchemy.orm import Mapped, mapped_column
from app.config.database import Base
from datetime import datetime



class InfoScript(Base):
    __tablename__ = "info_scripts"
    id: Mapped[int] = mapped_column(primary_key=True)
    name_script: Mapped[str]
    log: Mapped[str] = mapped_column(Text)
    date: Mapped[datetime | None] = mapped_column(default=datetime.utcnow())



    def __str__(self):
        return f'Name_Script: {self.name_script}'
