from typing import Annotated
from fastapi import APIRouter, Depends, HTTPException, Path, Request, Response, status
from fastapi.params import Query
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from app.config.access import site_access
from app.users.dependencies import get_current_user
from app.users.models import User
from app.config.flash import flash, get_flashed_messages
from app.ext.script.utils import cryptoRFC1751
from app.ext.script.schemas import SRFC1751


router = APIRouter(prefix='/script', tags=['Scripts'])
templates=Jinja2Templates(directory="app/templates/")
templates.env.globals['get_flashed_messages'] = get_flashed_messages


@router.get('/crypto/{data_text}_{data_crypto}',)
async def cryptoMainGet(
    request: Request,
    current_user: User = Depends(get_current_user),
    data_text: str = None,
    data_crypto: str = None,
):
    data_main = {'title': 'Крипто'}
    # data_crypto = cryptoRFC1751(data='AB:3A:04:87:F0:83:07:DC:B6:22:45:0C:CC:8E:5D:B9:3F:FE:C3:4C')
    data_main['crypto'] = data_crypto
    data_main['text'] = data_text

    return templates.TemplateResponse(
        'script/script_crypto.html',
        {
            'request': request,
            'user': current_user,
            'data_main': data_main,
        }
    )


@router.post('/crypto/{data_text}_{data_crypto}',)
async def cryptoMainPost(
    request: Request,
    current_user: User = Depends(get_current_user),
    data_form: SRFC1751=Depends(SRFC1751.as_form),
):
    data_crypto, msg = cryptoRFC1751(data=data_form.hash)
    flash(request, msg, 'alert-info')
    
    return RedirectResponse(
        request.url_for(
            'cryptoMainGet', 
            data_text=data_form.hash, 
            data_crypto=data_crypto
            ), 
        status_code=303
    )





