import binascii
from Crypto.Util import RFC1751




def cryptoRFC1751(data: str):
    fingerprint_str=data.replace(':','').replace(' ','').upper()
    msg: str = ''
    if (len(fingerprint_str) > 32):
        msg = 'Введенные данные обрезаны до 32 символов.'
        
    try:
        fingerprint_txt = RFC1751.key_to_english(binascii.unhexlify(fingerprint_str[:32]))
    except:
        fingerprint_txt = 'Упс, что-то пошло не так... Скрипт не смог обработать введеные данные.'

    return fingerprint_txt, msg

if __name__ == '__main__':
    data = cryptoRFC1751(data = 'AB:3A:04:87:F0:83:07:DC:B6:22:45:0C:CC:8E:5D:B9:3F:FE:C3:4C')
    # print(data)

    # import rfc1751
    # # data = rfc1751.bytes_to_words(binascii.unhexlify('AB3A0487F08307DCB622450CCC8E5DB9'))
    # # print(data)

    # print(binascii.unhexlify('AB3A0487F08307DCB622450CCC8E5DB9'))
    # print(rfc1751.bytes_to_words(b'\xab:\x04\x87\xf0\x83\x07\xdc\xb6"E\x0c\xcc\x8e]\xb9'))