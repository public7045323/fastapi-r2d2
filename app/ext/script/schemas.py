from pydantic import BaseModel
from fastapi import Form



class SRFC1751(BaseModel):
    hash: str

    @classmethod
    def as_form(
        cls,
        hash: str = Form(...),
    ):
        return cls(
            hash=hash,
        )
    
    class Config:
        from_attibutes = True